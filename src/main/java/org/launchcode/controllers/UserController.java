package org.launchcode.controllers;

import org.launchcode.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Jenn Cai
 * 10/12/17
 */

@Controller
@RequestMapping("user")
public class UserController extends AbstractController {

    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public String index(HttpServletRequest request, Model model, @PathVariable String username) {
        User user = userDao.findByUsername(username);
        User sessionUser = getUserFromSession(request.getSession());
        model.addAttribute("wishlistTitle", user.getUsername() + "'s Wishlists");
        model.addAttribute("wishlists", user.getWishlists());
        model.addAttribute("title", user.getUsername() + "'s Items");
        model.addAttribute("items", user.getItems());
        model.addAttribute("user", user);
        model.addAttribute("sessionUser", sessionUser);

        return "user-page";
    }
}