package org.launchcode.controllers;

import org.launchcode.models.User;
import org.launchcode.models.Wishlist;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by Jenn Cai
 * 10/29/17
 */

@Controller
@RequestMapping("wishlist")
public class WishlistController extends AbstractController{
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(HttpServletRequest request, Model model,
                        @RequestParam(value = "id", required = false) Integer id) {
        User sessionUser = getUserFromSession(request.getSession());
        if (id != null) {
            Wishlist wishlist = wishlistDao.findOne(id);
            model.addAttribute("title", wishlist.getName());
            model.addAttribute("wishlist", wishlist);
            model.addAttribute("owner", wishlist.getCreator());
            model.addAttribute("items", wishlist.getItems());
            return "wishlist-detail";
        }

        model.addAttribute("wishlistTitle", "All Wishlists");
        model.addAttribute("wishlists", wishlistDao.findAll());
        model.addAttribute("userTitle", "All Users");
        model.addAttribute("users", userDao.findAll());

        return "list-wishlists";

    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String addWishlistForm(Model model){
        model.addAttribute("title", "Create a Wishlist");
        model.addAttribute(new Wishlist());
        return "add-wishlist";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addWishlist(HttpServletRequest request, Model model,
                          @ModelAttribute @Valid Wishlist wishlist, Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Create a Wishlist");
            return "add-wishlist";
        }
        wishlist.setCreator(getUserFromSession(request.getSession()));
        wishlistDao.save(wishlist);
        return "redirect:/wishlist?id=" + wishlist.getId();
    }

}
