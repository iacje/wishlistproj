package org.launchcode.controllers;

import org.launchcode.models.Item;
import org.launchcode.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Jenn Cai
 * 10/26/17
 */

@Controller
@RequestMapping(value = "favorite")
public class FavoriteController extends AbstractController {

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addFavorite(Model model, int itemId, HttpServletRequest request) {
        Item item = itemDao.findOne(itemId);
        User user = getUserFromSession(request.getSession());
        user.addFaveItem(item);
        userDao.save(user);
        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public String removeFavorite(Model model, int itemId, HttpServletRequest request) {
        Item item = itemDao.findOne(itemId);
        User user = getUserFromSession(request.getSession());
        user.getFaveItems().remove(item);
        userDao.save(user);
        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }
}
