package org.launchcode.controllers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.launchcode.models.Item;
import org.launchcode.models.User;
import org.launchcode.models.Wishlist;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;

/**
 * Created by Jenn Cai
 * 10/12/17
 */

@Controller
@RequestMapping("item")
public class ItemController extends AbstractController {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(HttpServletRequest request, Model model,
                        @RequestParam(value = "id", required = false) Integer id) {
        User sessionUser = getUserFromSession(request.getSession());
        if (id != null) {
            Item item = itemDao.findOne(id);
            model.addAttribute("title", item.getTitle());
            model.addAttribute("item", item);
            model.addAttribute("isOwner", sessionUser == item.getOwner());
            return "item-detail";
        }

        model.addAttribute("title", "All Items");
        model.addAttribute("items", itemDao.findAll());
        model.addAttribute("sessionUser", sessionUser);
        return "items";

    }

//    @RequestMapping(value = "list", method = RequestMethod.GET)
//    public String list(HttpServletRequest request, Model model) {
//        model.addAttribute("title", "All Items");
//        model.addAttribute("items", itemDao.findAll());
//        model.addAttribute("sessionUser", getUserFromSession(request.getSession()));
//        return "list-items";
//
//    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String addItemForm(HttpServletRequest request, Model model, RedirectAttributes redirAttrs,
                              @RequestParam(value = "wishlistId", required = true) int wishlistId) {

        Wishlist wishlist = wishlistDao.findOne(wishlistId);
        if (getUserFromSession(request.getSession()) != wishlist.getCreator()) {
            redirAttrs.addFlashAttribute("message", "You do not have permission to modify that wishlist");
            return "redirect:/home";
        }
        model.addAttribute("title","Add an Item");
        model.addAttribute(new Item());
        return "add-item";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addItem(HttpServletRequest request, Model model, @RequestParam(value = "wishlistId", required = true) int wishlistId,
                          @ModelAttribute @Valid Item item, Errors errors) throws IOException {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Add an Item");
            return "add-item";
        }

        /*
        https://stackoverflow.com/questions/11656064/how-to-get-page-meta-title-description-images-like-facebook-attach-url-using
         */
        Document doc = Jsoup.connect(item.getUrl()).get();

        String title = null;
        Elements metaOgTitle = doc.select("meta[property=og:title]");
        if (metaOgTitle != null) {
            title = metaOgTitle.attr("content");
        }
        else {
            title = doc.title();
        }
        if (title != null) {
            item.setTitle(title);
        }

        String image = null;
        Elements metaOgImage = doc.select("meta[property=og:image]");
        if (metaOgImage != null) {
            image = metaOgImage.attr("content");
        }
        if (image != null) {
            item.setImage(image);
        }

        item.setOwner(getUserFromSession(request.getSession()));
        item.setWishlist(wishlistDao.findOne(wishlistId));
        itemDao.save(item);
        return "redirect:/item?id=" + item.getId();
    }

    @RequestMapping(value = "edit/{itemId}", method = RequestMethod.GET)
    public String editItemForm(HttpServletRequest request, Model model, @PathVariable int itemId, RedirectAttributes redirAttrs){
        Item item = itemDao.findOne(itemId);
        if (getUserFromSession(request.getSession()) != item.getOwner()) {
            redirAttrs.addFlashAttribute("message", "You do not have permission to edit that page");
            return "redirect:/home";
        }
        model.addAttribute("item", item);
        model.addAttribute("title", "Edit Item: " + item.getTitle());
        model.addAttribute("itemId", itemId);
        return "edit";
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String editItem(Model model, int itemId, @ModelAttribute @Valid Item newItem,
                                  Errors errors) {

        Item item = itemDao.findOne(itemId);
        if (errors.hasErrors()) {
            model.addAttribute("item", newItem);
            model.addAttribute("title", "Edit Item: " + item.getTitle());
            model.addAttribute("itemId", itemId);
            return "edit";
        }

        item.setTitle(newItem.getTitle());
        item.setImage(newItem.getImage());
        item.setDescription(newItem.getDescription());
        itemDao.save(item);
        return "redirect:/item?id=" + itemId;
    }

    @RequestMapping(value = "editTest", method = RequestMethod.POST)
    public String editTest(Model model, String description, int itemId){
        Item item = itemDao.findOne(itemId);
        item.setDescription(description);
        itemDao.save(item);
        return "redirect:/item/list";
    }
}
