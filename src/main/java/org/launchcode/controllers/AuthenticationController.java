package org.launchcode.controllers;

import org.launchcode.models.User;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by Jenn Cai
 * 10/16/17
 *
 * Code modified from: https://github.com/LaunchCodeEducation/blogz-spring/blob/master/src/main/java/org/launchcode/blogz/controllers/AuthenticationController.java
 */

@Controller
public class AuthenticationController extends AbstractController {

    @RequestMapping(value = "/sign-up", method = RequestMethod.GET)
    public String signUpForm(Model model){
        model.addAttribute("title", "Create an Account");
        model.addAttribute(new User());
        return "sign-up";
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public String signUp(HttpServletRequest request, Model model,
                         @ModelAttribute @Valid User user, Errors errors,
                         String confirm, RedirectAttributes redirAttrs) {

        User existing = userDao.findByUsername(user.getUsername());
        boolean isConfirmed = user.getPassword().equals(confirm);

        if (errors.hasErrors() || !isConfirmed || existing != null) {
            model.addAttribute("title", "Create an Account");
            model.addAttribute(user);
            if (existing != null) {
                model.addAttribute("usernameExists", "Username already exists");
            }
            if (!isConfirmed) {
                model.addAttribute("confirmError", "Passwords do not match");
            }
            return "sign-up";
        }
        user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
        userDao.save(user);
        setUserInSession(request.getSession(), user);
        redirAttrs.addFlashAttribute("message", "Welcome " + user.getUsername() + "!");
        return "redirect:/home";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginForm(Model model) {
        model.addAttribute("title", "Log Into Your Account");
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(HttpServletRequest request, Model model, String username, String password,
                        RedirectAttributes redirAttrs) {

        User user = userDao.findByUsername(username);

        if (user != null && BCrypt.checkpw(password, user.getPassword())) {
            setUserInSession(request.getSession(), user);
            redirAttrs.addFlashAttribute("message", "Welcome " + user.getUsername() + "!");
            return "redirect:/home";
        }

        model.addAttribute("title", "Log Into Your Account");
        model.addAttribute("error", "Username or password incorrect");
        model.addAttribute("username", username);
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request) {
        request.getSession().invalidate();
        return "redirect:/wishlist";
    }
}