package org.launchcode.controllers;

import org.launchcode.models.User;
import org.launchcode.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Jenn Cai
 * 10/12/17
 */

@Controller
public class HomeController extends AbstractController {

    @RequestMapping(value = "/home")
    public String userHome(HttpServletRequest request, Model model) {
        User user = getUserFromSession(request.getSession());
        model.addAttribute("title", "My Items");
        model.addAttribute("items", user.getItems());
        model.addAttribute("favorites", user.getFaveItems());
        model.addAttribute("faveTitle", "My Favorites");
        model.addAttribute("sessionUser", user);
        model.addAttribute("wishlists", user.getWishlists());
        model.addAttribute("wishlistTitle", "My Wishlists");
        return "user-home";
    }

}
