package org.launchcode.controllers;

import org.launchcode.models.User;
import org.launchcode.models.data.ItemDao;
import org.launchcode.models.data.UserDao;
import org.launchcode.models.data.WishlistDao;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;

/**
 * Created by Jenn Cai
 * 10/16/17
 *
 * Code modified from https://github.com/LaunchCodeEducation/blogz-spring/blob/master/src/main/java/org/launchcode/blogz/controllers/AbstractController.java
 */

public abstract class AbstractController {

    @Autowired
    protected UserDao userDao;

    @Autowired
    protected ItemDao itemDao;

    @Autowired
    protected WishlistDao wishlistDao;

    public static final String userSessionKey = "user_id";

    protected User getUserFromSession(HttpSession session) {

        Integer userId = (Integer) session.getAttribute(userSessionKey);
        return userId == null ? null : userDao.findOne(userId);
    }

    protected void setUserInSession(HttpSession session, User user) {
        session.setAttribute(userSessionKey, user.getId());
    }

}