package org.launchcode.models;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;


/**
 * Created by Jenn Cai
 * 10/12/17
 */

@Entity
public class User extends AbstractEntity{

    @NotNull
    @Size(min = 3, max = 20, message = "Username must be between 3 to 20 characters in length")
    @Column(unique = true)
    private String username;

//    @NotNull
//    @NotEmpty(message = "Please enter an email address")
//    @Email
//    private String email;

    @NotNull
    @Size(min = 4, message = "Password must be a minimum of 4 characters")
    private String password;

    @OneToMany
    @JoinColumn(name = "owner_id")
    private List<Item> items;

    @ManyToMany
    private List<Item> faveItems;

    @OneToMany
    @JoinColumn(name = "creator_id")
    private List<Wishlist> wishlists;

    public User(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    public User() {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Item> getItems() {
        return items;
    }

    public List<Item> getFaveItems() {
        return faveItems;
    }

    public void addFaveItem(Item item) {
        faveItems.add(item);
    }

    public List<Wishlist> getWishlists() {
        return wishlists;
    }
}
