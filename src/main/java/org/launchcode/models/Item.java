package org.launchcode.models;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Created by Jenn Cai
 * 10/12/17
 */

@Entity
public class Item extends AbstractEntity {

    @NotNull
    @NotEmpty
    @Size(max=1000)
    @URL(message = "Please enter a valid URL")
    private String url;

    @NotNull
    @NotEmpty(message = "Please enter a title")
    private String title = "No title available";

    @NotNull
    private String image;

    @NotNull
    @Size(max=200, message="Description has a max of 200 characters")
    private String description;

    private Date date = new Date();

    @ManyToOne
    private User owner;

    @ManyToMany(mappedBy = "faveItems")
    private List<User> favoritedBy;

    @ManyToOne
    private Wishlist wishlist;

    public Item(String url) {
        super();
        this.url = url;
    }

    public Item() {}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<User> getFavoritedBy() {
        return favoritedBy;
    }

    public void setFavoritedBy(List<User> favoritedBy) {
        this.favoritedBy = favoritedBy;
    }

    public Wishlist getWishlist() {
        return wishlist;
    }

    public void setWishlist(Wishlist wishlist) {
        this.wishlist = wishlist;
    }
}
