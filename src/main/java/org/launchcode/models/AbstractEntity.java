package org.launchcode.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * Created by Jenn Cai
 * 10/16/17
 *
 * Code modified from: https://github.com/LaunchCodeEducation/blogz-spring/blob/master/src/main/java/org/launchcode/blogz/models/AbstractEntity.java
 */

@MappedSuperclass
public abstract class AbstractEntity {

    @Id
    @GeneratedValue
    @NotNull
    @Column(unique = true)
    private int id;

    public int getId() {
        return this.id;
    }

    protected void setId(int id) {
        this.id = id;
    }

}