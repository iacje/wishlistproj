package org.launchcode.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Jenn Cai
 * 10/29/17
 */

@Entity
public class Wishlist extends AbstractEntity {
    @NotNull
    @Size(min = 1, max = 50, message = "Must be between 1 to 50 characters in length")
    @Column(unique = true)
    private String name;

    @ManyToOne
    private User creator;

    @OneToMany
    @JoinColumn(name = "wishlist_id")
    private List<Item> items;

    public Wishlist(String name) {
        this.name = name;
    }

    public Wishlist() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public List<Item> getItems() {
        return items;
    }
}
