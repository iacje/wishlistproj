package org.launchcode.models.data;

import org.launchcode.models.Wishlist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by Jenn Cai
 * 10/29/17
 */

@Repository
@Transactional
public interface WishlistDao extends CrudRepository<Wishlist,Integer> {
}
