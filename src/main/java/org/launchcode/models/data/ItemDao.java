package org.launchcode.models.data;

import org.launchcode.models.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by Jenn Cai
 * 10/12/17
 */

@Repository
@Transactional
public interface ItemDao extends CrudRepository<Item, Integer> {


}
