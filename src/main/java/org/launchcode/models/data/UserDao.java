package org.launchcode.models.data;

import org.launchcode.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Jenn Cai
 * 10/12/17
 *
 * Code modified from https://github.com/LaunchCodeEducation/blogz-spring/blob/master/src/main/java/org/launchcode/blogz/models/dao/UserDao.java
 */

@Repository
@Transactional
public interface UserDao extends CrudRepository<User,Integer> {
    User findByUsername(String username);
}
