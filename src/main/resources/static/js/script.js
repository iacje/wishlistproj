const $faveButton = $('<button class="fave btn btn-default btn-circle">');
$faveButton.append($('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true">'));
$(".favorite").append($faveButton);

const $unfaveButton = $('<button class="unfave btn btn-default btn-circle">');
$unfaveButton.append($('<span class="glyphicon glyphicon-heart" aria-hidden="true">'));
$(".unfavorite").append($unfaveButton);

function clickFave(event) {
    event.stopPropagation()
    $(this).attr("disabled", true);
    $(this).blur();
    var itemData = {
        itemId : $(this).parent().attr("id"),
    };
    $.ajax({
        type: "POST",
        url: $(this).hasClass("fave") ? "/favorite/add" : "/favorite/remove",
        data: itemData,
        context: this,
        success: function() {
            $(this).toggleClass("fave unfave");
            $(this).children(":first").toggleClass("glyphicon-heart-empty glyphicon-heart");
            $(this).parent().toggleClass("favorite unfavorite");
        },
        error: function() {
            alert("Unable to change favorite at this time. Please try again later!");
        }
    }).always(function() {
            $(this).attr("disabled", false);
    });
}

$(".fave, .unfave").on("click", clickFave);

$(window).bind("pageshow", function() {
    // $(".toggleDiv").empty();
    // $(".toggleDiv").append('<input class="myToggle" type="checkbox" data-toggle="toggle" data-on="&lt;span class=\'glyphicon glyphicon-picture\'&gt;&nbsp;Images&lt;span&gt;" data-off="&lt;span class=\'glyphicon glyphicon-th-list\'&gt;&nbsp;List&lt;span&gt;" data-onstyle="success" data-offstyle="default" checked="true"/>')
    $(".myToggle").parent().on("click", function() {
        $(".myWishlist").children().toggleClass("show hide");
    });
});